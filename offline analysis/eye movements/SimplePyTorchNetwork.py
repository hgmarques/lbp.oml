import pandas as pd
import matplotlib.pyplot as plt

import torch
import torch.nn as nn
import torch.optim as optim

# Define the training data
# Input variable (x) and corresponding target variable (y)
# x_train = torch.tensor([[1.0], [2.0], [3.0], [4.0]])
# y_train = torch.tensor([[2.0], [4.0], [6.0], [8.0]])


filename = "C:\\Users\\HugoMarques\\Carey Lab Dropbox\\Hugo Marques\\Hugo\\Projects\\LittleBrainProject\\Bonsai\\20230428 - Eye Movements using Velocity Profile\\EyeSensorimotorData.csv"

df = pd.read_csv(filename, names=["tx","ty","cx","cy","cxr","cyr","mx","my"])

#df[["cx","cy","mx","my"]] = df[["cx","cy","mx","my"]] / 255

df['s1'] = df['mx'].shift(1)
df['s2'] = df['mx'].shift(2)
df['s3'] = df['mx'].shift(3)
df['s4'] = df['mx'].shift(4)
df['s5'] = df['mx'].shift(5)
df['s6'] = df['mx'].shift(6)
df['s7'] = df['mx'].shift(7)
df['s8'] = df['mx'].shift(8)
df['s9'] = df['mx'].shift(9)
df['s10'] = df['mx'].shift(10)
df['s11'] = df['mx'].shift(11)
df['s12'] = df['mx'].shift(12)
df['s13'] = df['mx'].shift(13)
df['s14'] = df['mx'].shift(14)
df['s15'] = df['mx'].shift(15)
df['s16'] = df['mx'].shift(16)
df['s17'] = df['mx'].shift(17)
df['s18'] = df['mx'].shift(18)
df['s19'] = df['mx'].shift(19)
df['s20'] = df['mx'].shift(20)



df = df.drop(index=range(0, 31))
print(df.head())

df = df.astype("float32")

x_train = torch.Tensor(df[['mx','s1','s2','s3','s4','s5','s6','s7','s8','s9', 's10','s11','s12','s13','s14','s15','s16','s17','s18','s19','s20']].values)
y_train = torch.tensor(df[['cx','cy']].values)


def spatialExpansion(x, bf, minmax_range, gstd, is_circular):
    
    output = np.zeros((bf, 1))

    for (int i = 0; i < NumberOfNeurons; i++)
        bf_mean = i * (minmax_range[1] - minmax_range[0]) / (bf - 1) + minmax_range[0]
        output[i] = 1 / (gstd * Math.Sqrt(2 * math.pi)) * math.exp(- math.pow((x - bf_mean), 2) / (2 * gstd * gstd))
    
    return output

    
    
    
    
    



# Define the model
class SimpleModel(nn.Module):
    def __init__(self):
        super(SimpleModel, self).__init__()
        self.linear = nn.Linear(10, 2, bias=True)  # One input and one output

    def forward(self, x):
        return self.linear(x)
    
    
class BetterModel(nn.Module):
    def __init__(self):
        super(BetterModel, self).__init__()
        self.fc1 = nn.Linear(21, 40, bias=True)
        self.fc2 = nn.Linear(40, 40, bias=True)
        self.fc3 = nn.Linear(40, 2, bias=True)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        x = self.fc3(x)
        return x

model = BetterModel()

# Define the loss function and optimizer
criterion = nn.MSELoss()  # Mean Squared Error loss
optimizer = optim.SGD(model.parameters(), lr=0.01)  # Stochastic Gradient Descent optimizer


# Train the model
num_epochs = 2000
for epoch in range(num_epochs):
    # Forward pass
    outputs = model(x_train)
    loss = criterion(outputs, y_train)

    # Backward and optimize
    optimizer.zero_grad()  # Clear the gradients
    loss.backward()  # Compute gradients
    optimizer.step()  # Update weights

    # Print progress
    if (epoch + 1) % 100 == 0:
        print(f"Epoch [{epoch+1}/{num_epochs}], Loss: {loss.item():.4f}")

# Test the model
predicted = model(x_train)

%matplotlib qt
plt.plot(y_train[:,0])
pred = predicted.detach().numpy()
plt.plot(pred[:,0])


#print("Predicted values:")
#for i in range(len(x_test)):
#    print(f"Input: {x_test[i].item()}, Predicted: {predicted[i].item()}")



%matplotlib qt
plt.plot(y_train[:,0])
pred = predicted.detach().numpy()
plt.plot(pred[:,0])

